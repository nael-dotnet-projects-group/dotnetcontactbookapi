
IF OBJECT_ID(N'dbo.Addresses', N'U') IS NOT NULL
DROP TABLE Addresses;
GO

IF OBJECT_ID(N'dbo.PhoneContact', N'U') IS NOT NULL
DROP TABLE PhoneContact;
GO

IF OBJECT_ID(N'dbo.ContactPerson', N'U') IS NOT NULL
DROP TABLE ContactPerson;
GO


