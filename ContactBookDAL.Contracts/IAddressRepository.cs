using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.Entities;

namespace ContactBookDAL.Contracts
{
    public interface IAddressRepository
    {
        public Task<Address> Insert(DbContext dbContext, Address addresss);

        public Task InsertBatch(DbContext dbContext, List<Address> addresses);

        public Task<long> CountTotalRows(DbContext dbContext);

        public Task<List<Address>> FindByPersonId(DbContext dbContext, long personId);

        public Task<Address?> FindById(DbContext dbContext, long id);

        public Task<int> DeleteById(DbContext dbContext, long id);

        public Task<int> DeleteByPersonId(DbContext dbContext, long personId);

        public Task<int> Update(DbContext dbContext, Address address);

    }
}