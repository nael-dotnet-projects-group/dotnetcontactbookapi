using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.DTOs.PhoneContact;
using ContactBookDAL.Contracts.Entities;

namespace ContactBookDAL.Contracts
{
    public interface IContactPersonRepository
    {
        public Task<ContactPerson> Insert(DbContext dbContext, ContactPerson contacPerson);

        public Task InsertBatch(DbContext dbContext, List<ContactPerson> contactPeople);

        public Task<List<ContactPerson>> FindAll(DbContext dbContext, PersonPaginationQuery paginationQuery);

        public Task<long> CountTotalRows(DbContext dbContext);

        public Task<ContactPerson?> FindById(DbContext dbContext, long id);

        public Task DeleteById(DbContext dbContext, long id);

        public Task<ContactPerson> Update(DbContext dbContext, ContactPerson contactPerson);
    }
}