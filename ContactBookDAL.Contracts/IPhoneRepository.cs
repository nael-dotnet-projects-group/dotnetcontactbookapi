using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.Entities;

namespace ContactBookDAL.Contracts
{
    public interface IPhoneRepository
    {
        public Task<PhoneContact> Insert(DbContext dbContext, PhoneContact phoneContact);

        public Task InsertBatch(DbContext dbContext, List<PhoneContact> phoneContacts);

        public Task<List<PhoneContact>> FindByPersonId(DbContext dbContext, long personId);

        public Task<long> CountTotalRows(DbContext dbContext);

        public Task<PhoneContact?> FindById(DbContext dbContext, long id);

        public Task<int> DeleteById(DbContext dbContext, long id);

        public Task<int> DeleteByPersonId(DbContext dbContext, long personId);

        public Task<PhoneContact> Update(DbContext dbContext, PhoneContact address);
    }
}