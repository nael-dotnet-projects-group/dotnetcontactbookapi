using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace ContactBookDAL.Contracts.Configs
{
    public class DatabaseConfig: IDatabaseConfig
    {
        private readonly string? _connectionString;

        public DatabaseConfig(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqlConnection CreateSqlConnection()
        {
            return new SqlConnection(_connectionString);
        }
    }
}