using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace ContactBookDAL.Contracts.Configs
{
    public interface IDatabaseConfig
    {
        public SqlConnection CreateSqlConnection();
    }
}