using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace ContactBookDAL.Contracts.Configs
{
    public class DbContext(SqlConnection connection)
    {
        private readonly SqlConnection _connection = connection ?? throw new ArgumentNullException(nameof(connection));

        private SqlTransaction? _transaction;

        public SqlConnection Connection => _connection;

        public SqlTransaction? Transaction => _transaction;

        public async Task OpenConnectionAsync()
        {
            if(_connection.State == ConnectionState.Closed)
            {
                await _connection.OpenAsync();
            }
        }

        public async void BeginTransactionAsync()
        {
            if(_connection.State != ConnectionState.Open)
            {
                throw new InvalidOperationException("Connection must be open to begin a transaction.");
            }

            _transaction = (SqlTransaction) await _connection.BeginTransactionAsync();
        }

        public async Task CommitAsync()
        {
            if (_transaction == null)
            {
                throw new InvalidOperationException("No transaction to commit.");
            }

            await _transaction.CommitAsync();
        }

        public async Task RollbackAsync()
        {
            if (_transaction == null)
            {
                throw new InvalidOperationException("No transaction to rollback.");
            }

            await _transaction.RollbackAsync();
        }

        public async void DisposeAsync()
        {
            await _connection.DisposeAsync();
            if (_transaction != null)
                await _transaction.DisposeAsync();
        }
    }
}