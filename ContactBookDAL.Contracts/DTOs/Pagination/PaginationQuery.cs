using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.DTOs.Pagination.Enums;

namespace ContactBookDAL.Contracts.DTOs.Pagination
{
    public abstract class PaginationQuery
    {
        public long Limit { get; set; }

        public long Page { get; set; }

        public string? Keyword { get; set; }

        public AppSortOrder Order { get; set; }

        private string? _sortBy;

        public virtual string? SortBy
        {
            get => _sortBy;
            set => _sortBy = value;
        }


    }
}