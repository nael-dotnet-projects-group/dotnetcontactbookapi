using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.DTOs.Pagination;

namespace ContactBookDAL.Contracts.DTOs.Pagination
{
    public class PersonPaginationQuery: PaginationQuery
    {
        private string? _sortBy;
        public override string? SortBy 
        { 
            get
            {
                var propertiesDict = new Dictionary<string, string>
                {
                    {"Id", "ID"}
                    ,{"ID", "ID"}
                    ,{"FullName", "FullName"}
                    ,{"CreatedAt", "CreatedAt"}
                    ,{"UpdatedAt", "UpdatedAt"}
                };

                if(_sortBy != null && propertiesDict.ContainsKey(_sortBy))
                {
                    return _sortBy;
                }
                return "FullName";
            }
            set => _sortBy = value;
        }
    }
}