using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.DTOs.Pagination
{
    public class ResponsePagination<T>
    {
        public long Page { get; set; }

        public long TotalPage { get; set; }

        public long TotalItem { get; set; }

        public required List<T> Items { get; set; }

    }
}