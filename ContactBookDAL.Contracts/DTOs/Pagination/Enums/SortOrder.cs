namespace ContactBookDAL.Contracts.DTOs.Pagination.Enums
{
    public enum AppSortOrder
    {
        ASC, DESC
    }
}