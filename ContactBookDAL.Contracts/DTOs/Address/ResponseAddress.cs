using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.DTOs.Address
{
    public class ResponseAddress
    {
        public long Id { get; set; }

        public string? PostalCode { get; set; }

        public required string Address { get; set; }

        public string? City { get; set; }
    }
}