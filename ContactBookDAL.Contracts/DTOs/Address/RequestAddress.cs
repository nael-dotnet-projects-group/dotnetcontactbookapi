using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Entities;

namespace ContactBookDAL.Contracts.DTOs.Address
{
    public class RequestAddress
    {
        public string? PostalCode { get; set; }

        [Required]
        public required string Addresses { get; set; }

        public string? City { get; set; }

        public Entities.Address MapToAddress(long personId)
        {
            return new Entities.Address ()
            {
                Addresses = Addresses
                ,City = City
                ,PostalCode = PostalCode
                ,PersonId = personId
                ,CreatedAt = DateTime.Now
                ,UpdatedAt = DateTime.Now
            };
        }

    }
}