using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.DTOs.Person
{
    public class ResponsePersonSaved
    {
        public long Id { get; set; }

        public required string FullName { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}