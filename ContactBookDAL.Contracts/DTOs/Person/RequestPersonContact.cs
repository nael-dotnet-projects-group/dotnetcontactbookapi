using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.DTOs.Address;
using ContactBookDAL.Contracts.DTOs.PhoneContact;
using ContactBookDAL.Contracts.Entities;

namespace ContactBookDAL.Contracts.DTOs.Person
{
    public class RequestPersonContact
    {
        
        [Required]
        public required string FullName { get; set; }

        public List<RequestAddress> Addresses { get; set; } = [];

        public List<RequestPhone> Phones { get; set; } = [];

        public ContactPerson MapToContactPerson()
        {
            return new ContactPerson ()
            {
                FullName = FullName
                ,CreatedAt = DateTime.Now
                ,UpdatedAt = DateTime.Now
            };
        }


    }
}