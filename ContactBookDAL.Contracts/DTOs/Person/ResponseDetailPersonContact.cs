using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.DTOs.Address;
using ContactBookDAL.Contracts.DTOs.PhoneContact;

namespace ContactBookDAL.Contracts.DTOs.Person
{
    public class ResponseDetailPersonContact
    {
        public long Id { get; set; }
        
        public required string FullName { get; set; }

        public List<ResponseAddress> Addresses { get; set; } = [];

        public List<ResponsePhone> Phones { get; set; } = [];
    }
}