using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.DTOs.Person
{
    public class ResponsePersonContact
    {
        public long Id { get; set; }

        public string? FullName { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            ResponsePersonContact person = (ResponsePersonContact) obj;
            return Id.Equals(person.Id)
                && FullName == person.FullName;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, FullName);
        }
    }
}