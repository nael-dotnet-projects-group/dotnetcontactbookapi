using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.DTOs.PhoneContact
{
    public class ResponsePhone
    {
        public long Id { get; set; }
        
        public required string PhoneNumber { get; set; }

        public string? Description { get; set; }
    }
}