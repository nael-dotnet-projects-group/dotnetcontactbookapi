using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.DTOs.PhoneContact
{
    public class RequestPhone
    {
        [Required]
        public required string PhoneNumber { get; set; }

        public string? Description { get; set; }

        public Entities.PhoneContact MapToPhoneContact(long personId)
        {
            return new Entities.PhoneContact ()
            {
                PhoneNumber = PhoneNumber
                ,Description = Description
                ,PersonId = personId
                ,CreatedAt = DateTime.Now
                ,UpdatedAt = DateTime.Now
            };
        }
    }
}