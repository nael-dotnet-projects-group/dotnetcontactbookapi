using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.Entities
{
    public class PhoneContact 
    {
        public long Id { get; set; }
        public required string PhoneNumber { get; set; }
        public string? Description { get; set; }
        public long PersonId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            PhoneContact phoneContact = (PhoneContact) obj;
            return Id.Equals(phoneContact.Id)
                && PhoneNumber.Equals(phoneContact.PhoneNumber)
                && Description == phoneContact.Description
                && PersonId.Equals(phoneContact.PersonId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, PhoneNumber, Description, PersonId, CreatedAt, UpdatedAt);
        }
    }
}