using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.Entities
{
    public class Address
    {
        public long Id {get; set;}
        public required string Addresses {get; set;}
        public string? PostalCode {get; set;}
        public string? City {get; set;}
        public long PersonId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            Address address = (Address) obj;
            return Id.Equals(address.Id)
                    && Addresses.Equals(address.Addresses)
                    && PostalCode == address.PostalCode
                    && City == address.City;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Addresses, PostalCode, City);
        }
    }
}