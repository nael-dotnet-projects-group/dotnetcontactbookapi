using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactBookDAL.Contracts.Entities
{
    public class ContactPerson
    {
        public long Id { get; set; }
        public required string FullName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public override bool Equals(object? obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            ContactPerson contactPerson = (ContactPerson) obj;
            return Id.Equals(contactPerson.Id)
                && FullName.Equals(contactPerson.FullName);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, FullName, CreatedAt, UpdatedAt);
        }
    }
}