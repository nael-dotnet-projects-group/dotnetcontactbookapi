using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookBLL;
using ContactBookDAL.Contracts.DTOs.Person;
using ContactBookDAL.Contracts.Entities;
using Microsoft.AspNetCore.Mvc;

namespace ContactBookApi
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactPersonController : ControllerBase
    {
        private readonly IContactService _contactService;

        public ContactPersonController(IContactService contactService)
        {
            _contactService = contactService;
        }

        [HttpPost]
        public async Task<IActionResult> InsertNewContact([FromBody] RequestPersonContact dto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                var result = await _contactService.InsertContact(dto);
                return Ok(result);
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        // public async
        


    }
}