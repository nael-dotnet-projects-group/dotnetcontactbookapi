using ContactBookBLL;
using ContactBookDAL.Contracts;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.MSQL;
using Microsoft.Data.SqlClient;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Untuk equivalent spring boot "prototype" scope di C# ada builder.Services.AddTransient
// builder.Services.AddSingleton<IDatabaseConfig>(provider => new DatabaseConfig());
string? connectionString = builder.Configuration.GetConnectionString("MSQLConnectionString");


builder.Services.AddScoped<IDatabaseConfig>(provider => new DatabaseConfig(connectionString ?? ""));
builder.Services.AddScoped<IContactService, ContactService>();
builder.Services.AddSingleton<IAddressRepository, AddressRepository>();
builder.Services.AddSingleton<IPhoneRepository, PhoneRepository>();
builder.Services.AddSingleton<IContactPersonRepository, ContactPersonRepository>();

builder.Services.AddSingleton<DatabaseConfig>(dConf => {
    var connectionString = builder.Configuration.GetConnectionString("MSQLConnectionString");
    return new DatabaseConfig(connectionString!);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();
var logger = app.Services.GetRequiredService<ILogger<Program>>();
logger.LogInformation("connectionString: {connectionString}", connectionString);
app.Run();


