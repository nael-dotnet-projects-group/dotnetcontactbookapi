using System.Data;
using ContactBookDAL.Contracts;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.Entities;
using Microsoft.Data.SqlClient;

namespace ContactBookDAL.MSQL
{
    public class ContactPersonRepository : IContactPersonRepository
    {
        private readonly string _tableName = "ContactPerson";

        public async Task<long> CountTotalRows(DbContext dbContext)
        {
            string query = $"SELECT COUNT(*) FROM {_tableName}";
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            return Convert.ToInt64(await cmd.ExecuteScalarAsync());
        }

        public async Task DeleteById(DbContext dbContext, long id)
        {
            string query = $"DELETE FROM {_tableName} WHERE ID = @ID";
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@ID", id);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<ContactPerson>> FindAll(DbContext dbContext, PersonPaginationQuery paginationQuery)
        {
            string query = @$"
                SELECT * FROM ContactPerson
                WHERE (FullName LIKE @FullName)
                ORDER BY {paginationQuery.SortBy} {paginationQuery.Order}
                OFFSET @PageSize * (@PageNumber - 1) ROWS
                FETCH NEXT @PageSize ROWS ONLY;
            ";

            var persons = new List<ContactPerson>();

            SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PageSize", paginationQuery.Limit);
            command.Parameters.AddWithValue("@PageNumber", paginationQuery.Page);
            command.Parameters.AddWithValue("@FullName", $"%{paginationQuery.Keyword}%");
            // TODO: Lanjut pagination

            using (SqlDataReader reader = await command.ExecuteReaderAsync())
            {
                while(await reader.ReadAsync())
                {
                    var personContact = new ContactPerson
                    {
                        Id = reader.GetInt64(reader.GetOrdinal("ID"))
                        ,FullName = reader.GetString(reader.GetOrdinal("FullName"))
                        ,CreatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt"))
                        ,UpdatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt"))
                    };

                    persons.Add(personContact);
                }
            }

            return persons;
        }

        public async Task<ContactPerson?> FindById(DbContext dbContext, long id)
        {
            string query = @$"
                SELECT TOP 1
                    ID, FullName, CreatedAt, UpdatedAt
                FROM {_tableName}
                WHERE ID = @ID
            ";

            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@ID", id);

            using SqlDataReader reader = await cmd.ExecuteReaderAsync();
            if(await reader.ReadAsync())
            {
                return new ContactPerson
                {
                     Id = reader.GetInt64(reader.GetOrdinal("ID")),
                     FullName = reader.GetString(reader.GetOrdinal("FullName")),
                     CreatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt")),
                     UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt"))
                };
            }

            return null;
        }

        public async Task<ContactPerson> Insert(DbContext dbContext, ContactPerson contactPerson)
        {
            string query = @$"
                INSERT INTO {_tableName}(FullName, CreatedAt, UpdatedAt)
                VALUES (@FullName, @CreatedAt, @UpdatedAt);
                SELECT SCOPE_IDENTITY();
            ";

            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@FullName", contactPerson.FullName);
            cmd.Parameters.AddWithValue("@CreatedAt", contactPerson.CreatedAt);
            cmd.Parameters.AddWithValue("@UpdatedAt", contactPerson.UpdatedAt);
            long generatedKey = Convert.ToInt64(await cmd.ExecuteScalarAsync());
            contactPerson.Id = generatedKey;
            return contactPerson;
        }

        public async Task InsertBatch(DbContext dbContext, List<ContactPerson> contactPeople)
        {
            using var bulkCopy = new SqlBulkCopy(dbContext.Connection, SqlBulkCopyOptions.Default, dbContext.Transaction);
            bulkCopy.DestinationTableName = _tableName;

            bulkCopy.ColumnMappings.Add("FullName", "FullName");
            bulkCopy.ColumnMappings.Add("CreatedAt", "CreatedAt");
            bulkCopy.ColumnMappings.Add("UpdatedAt", "UpdatedAt");

            var table = new DataTable();
            table.Columns.Add("FullName", typeof(string));
            table.Columns.Add("CreatedAt", typeof(DateTime));
            table.Columns.Add("UpdatedAt", typeof(DateTime));

            foreach(var person in contactPeople)
            {
                table.Rows.Add(person.FullName, person.CreatedAt, person.UpdatedAt);
            }

            await bulkCopy.WriteToServerAsync(table);
        }

        public async Task<ContactPerson> Update(DbContext dbContext, ContactPerson contactPerson)
        {
            string query = @$"
                UPDATE {_tableName}
                SET FullName = @FullName
                    ,UpdatedAt = @UpdatedAt
                WHERE ID = @ID
            ";
            
            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@FullName", contactPerson.FullName);
            command.Parameters.AddWithValue("@UpdatedAt", contactPerson.UpdatedAt);
            command.Parameters.AddWithValue("@ID", contactPerson.Id);

            await command.ExecuteNonQueryAsync();
            return contactPerson;
        }
    }
}