using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace ContactBookDAL.MSQL.Util
{
    public class Transactional
    {
        public static async Task<T> Execute<T>(SqlConnection connection, Func<SqlTransaction, Task<T>> sqlTask) 
        {
            using(connection){
                await connection.OpenAsync();
                SqlTransaction transaction = (SqlTransaction) await connection.BeginTransactionAsync();
                try
                {
                    T result = await sqlTask(transaction);
                    transaction.Commit();
                    return result;
                }
                catch (System.Exception)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }




    }
}