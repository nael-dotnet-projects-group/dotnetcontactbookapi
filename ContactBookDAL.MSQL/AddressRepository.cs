using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ContactBookDAL.Contracts;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.Entities;
using ContactBookDAL.MSQL.Util;
using Microsoft.Data.SqlClient;

namespace ContactBookDAL.MSQL
{
    public class AddressRepository: IAddressRepository
    {
        private readonly string _tableName = "Addresses";

        public async Task<int> DeleteById(DbContext dbContext, long id)
        {
            string query = $"DELETE FROM {_tableName} WHERE ID = @ID";
            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@ID", id);
            return await command.ExecuteNonQueryAsync();
            
        }

        public async Task<List<Address>> FindByPersonId(DbContext dbContext, long personId)
        {
            string query = @$"
                SELECT TOP 100 
                    ID, PostalCode, Address, City, PersonId, CreatedAt, UpdatedAt
                from {_tableName}
                WHERE PersonId = @PersonId
            ";

            var addresses = new List<Address>();

            SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PersonId", personId);
            using (SqlDataReader reader = await command.ExecuteReaderAsync())
            {
                while(await reader.ReadAsync())
                {
                    var address = new Address 
                    {
                        Id = reader.GetInt64(reader.GetOrdinal("ID"))
                        ,PostalCode = reader.GetString(reader.GetOrdinal("PostalCode"))
                        ,Addresses = reader.GetString(reader.GetOrdinal("Address"))
                        ,City = reader.GetString(reader.GetOrdinal("City"))
                        ,PersonId = reader.GetInt64(reader.GetOrdinal("PersonId"))
                        ,CreatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt"))
                        ,UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt"))
                    };

                    addresses.Add(address);
                }
            }
            
            return addresses;
        }

        public async Task<Address?> FindById(DbContext dbContext, long id)
        {
            string query = @$"
                SELECT TOP 1 
                    ID, PostalCode, Address, City, PersonId, CreatedAt, UpdatedAt
                FROM {_tableName}
                WHERE ID = @ID
            ";

            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@ID", id);

            using SqlDataReader reader = await cmd.ExecuteReaderAsync();
            if(await reader.ReadAsync())
            {
                return new Address
                {
                    Id = reader.GetInt64(reader.GetOrdinal("ID")),
                    PostalCode = reader.GetString(reader.GetOrdinal("PostalCode")),
                    Addresses = reader.GetString(reader.GetOrdinal("Address")),
                    City = reader.GetString(reader.GetOrdinal("City")),
                    PersonId = reader.GetInt64(reader.GetOrdinal("PersonId")),
                    CreatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt")),
                    UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt"))
                };
            }

            return null;
        }

        public async Task<Address> Insert(DbContext dbContext, Address address)
        {
            string query = @$"
                INSERT INTO {_tableName}(PostalCode, Address, City, PersonId, CreatedAt, UpdatedAt)
                VALUES (@PostalCode, @Address, @City, @PersonId, @CreatedAt, @UpdatedAt);
                SELECT SCOPE_IDENTITY();
            ";

            // TODO: Test prosees insert ini
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@PostalCode", address.PostalCode!);
            cmd.Parameters.AddWithValue("@Address", address.Addresses);
            cmd.Parameters.AddWithValue("@City", address.City!);
            cmd.Parameters.AddWithValue("@PersonId", address.PersonId);
            cmd.Parameters.AddWithValue("@CreatedAt", address.CreatedAt!);
            cmd.Parameters.AddWithValue("@UpdatedAt", address.UpdatedAt!);
            long generatedKey = Convert.ToInt64(await cmd.ExecuteScalarAsync());
            address.Id = generatedKey;
            return address;
        }

        public async Task InsertBatch(DbContext dbContext, List<Address> addresses)
        {
            using var bulkCopy = new SqlBulkCopy(dbContext.Connection, SqlBulkCopyOptions.Default, dbContext.Transaction);
            bulkCopy.DestinationTableName = "Addresses";

            bulkCopy.ColumnMappings.Add("PostalCode", "PostalCode");
            bulkCopy.ColumnMappings.Add("Address", "Address");
            bulkCopy.ColumnMappings.Add("City", "City");
            bulkCopy.ColumnMappings.Add("PersonId", "PersonId");
            bulkCopy.ColumnMappings.Add("CreatedAt", "CreatedAt");
            bulkCopy.ColumnMappings.Add("UpdatedAt", "UpdatedAt");

            var table = new DataTable();
            table.Columns.Add("PostalCode", typeof(string));
            table.Columns.Add("Address", typeof(string));
            table.Columns.Add("City", typeof(string));
            table.Columns.Add("PersonId", typeof(long));
            table.Columns.Add("CreatedAt", typeof(DateTime));
            table.Columns.Add("UpdatedAt", typeof(DateTime));

            foreach(var address in addresses)
            {
                table.Rows.Add(address.PostalCode, address.Addresses, address.City, address.PersonId, address.CreatedAt, address.UpdatedAt);
            }


            await bulkCopy.WriteToServerAsync(table);
        }

        public async Task<int> Update(DbContext dbContext, Address address)
        {
            string query = @$"
                UPDATE {_tableName}
                SET PostalCode = @PostalCode
                    ,Address = @Address
                    ,City = @City
                    ,PersonId = @PersonId
                    ,UpdatedAt = @UpdatedAt
                WHERE ID = @ID
            ";

            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PostalCode", address.PostalCode);
            command.Parameters.AddWithValue("@Address", address.Addresses);
            command.Parameters.AddWithValue("@City", address.City);
            command.Parameters.AddWithValue("@PersonId", address.PersonId);
            command.Parameters.AddWithValue("@UpdatedAt", address.UpdatedAt);
            command.Parameters.AddWithValue("@ID", address.Id);
            return await command.ExecuteNonQueryAsync();
        }

        public async Task<long> CountTotalRows(DbContext dbContext)
        {
            string query = $"SELECT COUNT(*) FROM {_tableName}";
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            return Convert.ToInt64(await cmd.ExecuteScalarAsync());
        }

        public async Task<int> DeleteByPersonId(DbContext dbContext, long personId)
        {
            string query = $"DELETE FROM {_tableName} WHERE PersonId = @PersonId";
            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PersonId", personId);
            return await command.ExecuteNonQueryAsync();
        }
    }
}