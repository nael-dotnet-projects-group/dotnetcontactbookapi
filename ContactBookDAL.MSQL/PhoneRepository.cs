using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ContactBookDAL.Contracts;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.Entities;
using Microsoft.Data.SqlClient;

namespace ContactBookDAL.MSQL
{
    public class PhoneRepository : IPhoneRepository
    {
        private readonly string _tableName = "PhoneContact";

        public async Task<int> DeleteById(DbContext dbContext, long id)
        {
            string query = $"DELETE FROM {_tableName} WHERE ID = @ID";

            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@ID", id);
            return await command.ExecuteNonQueryAsync();
        }

        public async Task<List<PhoneContact>> FindByPersonId(DbContext dbContext, long personId)
        {
            string query = @$"
                SELECT TOP 100 
                    ID
                    ,PhoneNumber
                    ,Description
                    ,PersonId
                    ,CreatedAt
                    ,UpdatedAt
                FROM {_tableName}
                WHERE PersonId = @PersonId
            ";

            List<PhoneContact> phoneContacts = [];

            var command = new SqlCommand(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PersonId", personId);
            using (SqlDataReader reader = await command.ExecuteReaderAsync())
            {
                while(await reader.ReadAsync())
                {
                    var phone = new PhoneContact
                    {
                        Id = reader.GetInt64(reader.GetOrdinal("ID"))
                        ,PhoneNumber = reader.GetString(reader.GetOrdinal("PhoneNumber"))
                        ,Description = reader.GetString(reader.GetOrdinal("Description"))
                        ,PersonId = reader.GetInt64(reader.GetOrdinal("PersonId"))
                        ,CreatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt"))
                        ,UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt"))
                    };

                    phoneContacts.Add(phone);
                }
            }

            return phoneContacts;
        }

        public async Task<PhoneContact?> FindById(DbContext dbContext, long id)
        {
            string query = @$"
                SELECT TOP 1 
                    ID
                    ,PhoneNumber
                    ,Description
                    ,PersonId
                    ,CreatedAt
                    ,UpdatedAt
                FROM {_tableName}
                WHERE ID = @ID
            ";
            
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@ID", id);
            
            using SqlDataReader reader = await cmd.ExecuteReaderAsync();
            if(await reader.ReadAsync())
            {
                return new PhoneContact
                {
                    Id = reader.GetInt64(reader.GetOrdinal("ID"))
                    ,PhoneNumber = reader.GetString(reader.GetOrdinal("PhoneNumber"))
                    ,Description = reader.GetString(reader.GetOrdinal("Description"))
                    ,PersonId = reader.GetInt64(reader.GetOrdinal("PersonId"))
                    ,CreatedAt = reader.GetDateTime(reader.GetOrdinal("CreatedAt"))
                    ,UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt"))
                };
            }

            return null;
        }

        public async Task<PhoneContact> Insert(DbContext dbContext, PhoneContact phoneContact)
        {
            string query = @$"
                INSERT INTO {_tableName}
                (
                    [PhoneNumber],
                    [Description],
                    [PersonId],
                    [CreatedAt],
                    [UpdatedAt]
                ) VALUES (
                    @PhoneNumber,
                    @Description,
                    @PersonId,
                    @CreatedAt,
                    @UpdatedAt
                );
                SELECT SCOPE_IDENTITY();
            ";

            // TODO: Lanjut coba custom transaction pake windsor, bikin dulu insert untuk PhoneContact
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            cmd.Parameters.AddWithValue("@PhoneNumber", phoneContact.PhoneNumber);
            cmd.Parameters.AddWithValue("@Description", phoneContact.Description);
            cmd.Parameters.AddWithValue("@PersonId", phoneContact.PersonId);
            cmd.Parameters.Add("@CreatedAt", SqlDbType.DateTime).Value = phoneContact.CreatedAt;
            cmd.Parameters.Add("@UpdatedAt", SqlDbType.DateTime).Value = phoneContact.UpdatedAt;

            long generatedKey = Convert.ToInt64(await cmd.ExecuteScalarAsync());
            phoneContact.Id = generatedKey;
            return phoneContact;
        }

        public async Task InsertBatch(DbContext dbContext, List<PhoneContact> phoneContacts)
        {
            using var bulkCopy = new SqlBulkCopy(dbContext.Connection, SqlBulkCopyOptions.Default, dbContext.Transaction);
            bulkCopy.DestinationTableName = "PhoneContact";

            bulkCopy.ColumnMappings.Add("PhoneNumber", "PhoneNumber");
            bulkCopy.ColumnMappings.Add("Description", "Description");
            bulkCopy.ColumnMappings.Add("PersonId", "PersonId");
            bulkCopy.ColumnMappings.Add("CreatedAt", "CreatedAt");
            bulkCopy.ColumnMappings.Add("UpdatedAt", "UpdatedAt");

            var table = new DataTable();
            table.Columns.Add("PhoneNumber", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("PersonId", typeof(long));
            table.Columns.Add("CreatedAt", typeof(DateTime));
            table.Columns.Add("UpdatedAt", typeof(DateTime));

            foreach(var phone in phoneContacts)
            {
                table.Rows.Add(phone.PhoneNumber, 
                               phone.Description, 
                               phone.PersonId, 
                               phone.CreatedAt, 
                               phone.UpdatedAt);
            }

            await bulkCopy.WriteToServerAsync(table);
        }

        public async Task<PhoneContact> Update(DbContext dbContext, PhoneContact phoneContact)
        {
            string query = @$"
                UPDATE {_tableName}
                SET PhoneNumber = @PhoneNumber
                    ,Description = @Description
                    ,PersonId = @PersonId
                    ,UpdatedAt = @UpdatedAt
                WHERE ID = @ID
            ";
            
            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PhoneNumber", phoneContact.PhoneNumber);
            command.Parameters.AddWithValue("@Description", phoneContact.Description);
            command.Parameters.AddWithValue("@PersonId", phoneContact.PersonId);
            command.Parameters.AddWithValue("@UpdatedAt", phoneContact.UpdatedAt);
            command.Parameters.AddWithValue("@ID", phoneContact.Id);

            await command.ExecuteNonQueryAsync();
            return phoneContact;
        }

        public async Task<long> CountTotalRows(DbContext dbContext)
        {
            string query = $"SELECT COUNT(*) FROM {_tableName}";
            using SqlCommand cmd = new(query, dbContext.Connection, dbContext.Transaction);
            return Convert.ToInt64(await cmd.ExecuteScalarAsync());
        }

        public async Task<int> DeleteByPersonId(DbContext dbContext, long personId)
        {
            string query = $"DELETE FROM {_tableName} WHERE PersonId = @PersonId";

            using SqlCommand command = new(query, dbContext.Connection, dbContext.Transaction);
            command.Parameters.AddWithValue("@PersonId", personId);
            return await command.ExecuteNonQueryAsync();
        }
    }
}