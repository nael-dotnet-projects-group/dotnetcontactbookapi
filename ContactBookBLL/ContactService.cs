using System.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.Entities;
using ContactBookDAL.MSQL;
using Microsoft.Data.SqlClient;
using ContactBookDAL.Contracts.DTOs.Person;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.DTOs.PhoneContact;
using ContactBookDAL.Contracts;
using ContactBookDAL.Contracts.DTOs.Address;

namespace ContactBookBLL
{
    public class ContactService: IContactService
    {
        private readonly IDatabaseConfig _dbConfig;

        private readonly IContactPersonRepository _contactPersonRepo;

        private readonly IAddressRepository _addressRepo;

        private readonly IPhoneRepository _phoneRepo;

        public ContactService(IDatabaseConfig dbConfig, IAddressRepository addressRepo, IPhoneRepository phoneRepo, IContactPersonRepository contactPersonRepo)
        {
            _dbConfig = dbConfig;
            _contactPersonRepo = contactPersonRepo;
            _addressRepo = addressRepo;
            _phoneRepo = phoneRepo;
        }

        // TODO: Bikin unit test untuk update delete service
        public async Task UpdateAddress(long addressId, RequestAddress dto)
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                await _addressRepo.Update(dbContext, new Address {
                    Id = addressId
                    ,Addresses = dto.Addresses
                    ,PostalCode = dto.PostalCode
                    ,City = dto.City
                    ,UpdatedAt = now
                });
                
                await dbContext.CommitAsync();
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        }

        public async Task UpdatePhone(long phoneId, RequestPhone dto)
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                await _phoneRepo.Update(dbContext, new PhoneContact {
                    Id = phoneId
                    ,PhoneNumber = dto.PhoneNumber
                    ,Description = dto.Description
                    ,UpdatedAt = now
                });
                
                await dbContext.CommitAsync();
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        }


        public async Task UpdateContact(long contactId, RequestPersonContact dto) 
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                await _contactPersonRepo.Update(dbContext, new ContactPerson {
                    Id = contactId
                    ,FullName = dto.FullName
                    ,UpdatedAt = now
                });
                
                await dbContext.CommitAsync();
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        }

        public async Task DeleteContact(long contactId)
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                await _contactPersonRepo.DeleteById(dbContext, contactId);
                await _addressRepo.DeleteByPersonId(dbContext, contactId);
                await _phoneRepo.DeleteByPersonId(dbContext, contactId);
                
                await dbContext.CommitAsync();
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        }

        public async Task<ResponseDetailPersonContact?> GetContactDetail(long contactId)
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                ContactPerson? contactPerson = await _contactPersonRepo.FindById(dbContext, contactId);
                
                if(contactPerson == null) return null;
                
                List<Address> addresses = await _addressRepo.FindByPersonId(dbContext, contactId);

                List<ResponseAddress> responseAddresses = addresses
                        .Select(a => new ResponseAddress{
                            Id = a.Id
                            ,Address = a.Addresses
                            ,PostalCode = a.PostalCode
                            ,City = a.City
                        }).ToList();

                List<PhoneContact> phoneContacts = await _phoneRepo.FindByPersonId(dbContext, contactId);

                List<ResponsePhone> responsePhones = phoneContacts
                        .Select(p => new ResponsePhone {
                            Id = p.Id
                            ,PhoneNumber = p.PhoneNumber
                            ,Description = p.Description
                        }).ToList();

                await dbContext.CommitAsync();

                return new ResponseDetailPersonContact {
                    Id = contactPerson.Id
                    ,FullName = contactPerson.FullName
                    ,Addresses = responseAddresses
                    ,Phones = responsePhones
                };
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        } 

        public async Task<ResponsePagination<ResponsePersonContact>> GetContactPaginated(PersonPaginationQuery paginationQuery)
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                long totalItems = await _contactPersonRepo.CountTotalRows(dbContext);
                long totalPage = (long) Math.Ceiling((decimal) totalItems / paginationQuery.Limit);
                paginationQuery.Page = totalPage < paginationQuery.Page ? totalPage : paginationQuery.Page;
                List<ContactPerson> contacts = await _contactPersonRepo.FindAll(dbContext, paginationQuery);
                List<ResponsePersonContact> responseContacts = contacts.Select(c => {
                    return new ResponsePersonContact
                    {
                        Id = c.Id
                        ,FullName = c.FullName
                    };
                }).ToList();

                await dbContext.CommitAsync();

                return new ResponsePagination<ResponsePersonContact>()
                {
                    Page = paginationQuery.Page
                    ,TotalPage = totalPage
                    ,TotalItem = totalItems
                    ,Items = responseContacts
                };
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        }

        public async Task<ResponsePersonSaved> InsertContact(RequestPersonContact dto)
        {
            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            await dbContext.OpenConnectionAsync();

            dbContext.BeginTransactionAsync();
            DateTime now = DateTime.Now;
            try
            {
                ContactPerson contactPerson = await _contactPersonRepo.Insert(dbContext, dto.MapToContactPerson());

                List<PhoneContact> phoneContactParams = dto.Phones.Select(p => p.MapToPhoneContact(contactPerson.Id)).ToList();

                await _phoneRepo.InsertBatch(dbContext, phoneContactParams);

                List<Address> addressesParam = dto.Addresses.Select(a => a.MapToAddress(contactPerson.Id)).ToList();

                await _addressRepo.InsertBatch(dbContext, addressesParam);

                await dbContext.CommitAsync();

                return new ResponsePersonSaved {
                    Id = contactPerson.Id
                    ,FullName = contactPerson.FullName
                    ,CreatedAt = contactPerson.CreatedAt
                };
            }
            catch (System.Exception)
            {
                await dbContext.RollbackAsync();
                throw;
            } finally {
                dbContext.DisposeAsync();
            }
        }
    }
}