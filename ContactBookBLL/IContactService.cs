using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.DTOs.Address;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.DTOs.Person;
using ContactBookDAL.Contracts.DTOs.PhoneContact;

namespace ContactBookBLL
{
    public interface IContactService
    {
        public Task UpdateAddress(long addressId, RequestAddress dto);

        public Task UpdatePhone(long phoneId, RequestPhone dto);

        public Task UpdateContact(long contactId, RequestPersonContact dto);

        public Task DeleteContact(long contactId);

        public Task<ResponseDetailPersonContact?> GetContactDetail(long contactId);

        public Task<ResponsePagination<ResponsePersonContact>> GetContactPaginated(PersonPaginationQuery paginationQuery);

        public Task<ResponsePersonSaved> InsertContact(RequestPersonContact dto);

        
    }
}