using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.Entities;
using ContactBookDAL.MSQL;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit.Abstractions;
using Microsoft.Data.SqlClient;
using System.Data;
using Xunit.Sdk;


namespace ContactBookTest.ContactBookDAL
{
    public class AddressRepositoryTest
    {
        private DatabaseConfig _dbConfig;

        private readonly ITestOutputHelper _output;

        public AddressRepositoryTest(ITestOutputHelper output)
        {
            _dbConfig = new DatabaseConfig("server=NDS-LPT-0462\\SQL2019;Database=ContactBookDB;User Id=nael_projects_admin;Password=Nael2024;Integrated Security=True;MultipleActiveResultSets=True;TrustServerCertificate=True;");
            _output = output;
        }

        [Fact]
        public async void InsertTest()
        {
            DateTime now = DateTime.Now;
            var paramAddress = new Address() {
                Addresses = "Jl Pinus",
                City="Bandung",
                PostalCode="1234",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            AddressRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            Address result = await repo.Insert(dbContext, paramAddress);
            Assert.True(result.Id > 0);
            Address? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.True(result.Equals(readerResult));
            await dbContext.RollbackAsync();
            // await dbContext.CommitAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void UpdateTest()
        {
            DateTime now = DateTime.Now;
            var paramAddress = new Address() {
                Addresses = "Jl Pinus",
                City="Bandung",
                PostalCode="1234",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            AddressRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            Address result = await repo.Insert(dbContext, paramAddress);
            Assert.True(result.Id > 0);
            Address? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.True(result.Equals(readerResult));
            readerResult.Addresses = "Jl Cemara";
            await repo.Update(dbContext, readerResult);
            
            readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.Equal("Jl Cemara", readerResult.Addresses);

            await dbContext.RollbackAsync();
            // await dbContext.CommitAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void InsertBatchTest()
        {
            DateTime now = DateTime.Now;
            var paramAddress = new Address() {
                Addresses = "Jl Pinus",
                City="Bandung",
                PostalCode="1234",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            var paramAddress2 = new Address() {
                Addresses = "Jl Cendana",
                City="Bandung",
                PostalCode="4321",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            List<Address> addresses = [paramAddress, paramAddress2];

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            AddressRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);
            
            dbContext.BeginTransactionAsync();

            _ = repo.InsertBatch(dbContext, addresses);

            List<Address> result = await repo.FindByPersonId(dbContext, 2);
            Assert.Equal(2, result.Count);
            Address? subResult1 = result.FirstOrDefault(a => a.Addresses.Equals(paramAddress.Addresses));
            Assert.NotNull(subResult1);
            Assert.Equal(paramAddress.PostalCode, subResult1.PostalCode);
            Assert.Equal(paramAddress.Addresses, subResult1.Addresses);
            Assert.Equal(paramAddress.City, subResult1.City);

            Address? subResult2 = result.FirstOrDefault(a => a.Addresses.Equals(paramAddress2.Addresses));
            Assert.NotNull(subResult2);
            Assert.Equal(paramAddress2.PostalCode, subResult2.PostalCode);
            Assert.Equal(paramAddress2.Addresses, subResult2.Addresses);
            Assert.Equal(paramAddress2.City, subResult2.City);

            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void DeleteTest()
        {
            DateTime now = DateTime.Now;
            var paramAddress = new Address() {
                Addresses = "Jl Pinus",
                City="Bandung",
                PostalCode="1234",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            AddressRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            Address result = await repo.Insert(dbContext, paramAddress);
            Assert.True(result.Id > 0);
            Address? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);

            await repo.DeleteById(dbContext, result.Id);

            readerResult = await repo.FindById(dbContext, result.Id);
            Assert.Null(readerResult);

            await dbContext.RollbackAsync();
            // await dbContext.CommitAsync();
            dbContext.DisposeAsync();
        }
    }
}