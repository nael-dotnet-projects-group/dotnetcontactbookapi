using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.DTOs.Pagination.Enums;
using ContactBookDAL.Contracts.DTOs.PhoneContact;
using ContactBookDAL.Contracts.Entities;
using ContactBookDAL.MSQL;
using Microsoft.Data.SqlClient;
using Xunit.Abstractions;

namespace ContactBookTest.ContactBookDAL
{
    public class ContactPersonRepositoryTest
    {
        private DatabaseConfig _dbConfig;

        private readonly ITestOutputHelper _output;

        public ContactPersonRepositoryTest(ITestOutputHelper output)
        {
            _dbConfig = new DatabaseConfig("server=NDS-LPT-0462\\SQL2019;Database=ContactBookDB;User Id=nael_projects_admin;Password=Nael2024;Integrated Security=True;MultipleActiveResultSets=True;TrustServerCertificate=True;");
            _output = output;
        }

        [Fact]
        public async void InsertTest()
        {
            DateTime now = DateTime.Now;
            var paramPerson = new ContactPerson() {
                FullName = "Nael",
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            ContactPersonRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(System.Data.ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            ContactPerson result = await repo.Insert(dbContext, paramPerson);
            Assert.True(result.Id > 0);
            ContactPerson? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.Equal(paramPerson.FullName, readerResult.FullName);
            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void PaginationTest()
        {
            DateTime now = DateTime.Now;

            List<ContactPerson> contactPeople = [];
            for(int i = 0 ; i < 20 ; i++) {
                contactPeople.Add(new ContactPerson
                {
                     FullName = "Test " + i
                    ,CreatedAt = now
                    ,UpdatedAt = now
                });
            }

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            ContactPersonRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(System.Data.ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            await repo.InsertBatch(dbContext, contactPeople);
            
            List<ContactPerson> getPaginated = await repo.FindAll(dbContext, 
                new PersonPaginationQuery {
                    Limit = 5
                    ,Page = 2
                    ,Keyword = "Test"
                    ,Order=AppSortOrder.ASC
                    ,SortBy="ID"
                });

            Assert.Equal(5, getPaginated.Count);

            ContactPerson? firstRow = getPaginated.FirstOrDefault();

            Assert.NotNull(firstRow);

            Assert.Equal("Test 5", firstRow.FullName);
            

            await dbContext.RollbackAsync();
            // await dbContext.CommitAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void DeleteTest()
        {
            DateTime now = DateTime.Now;
            var paramPerson = new ContactPerson() {
                FullName = "Nael",
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            ContactPersonRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(System.Data.ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            ContactPerson result = await repo.Insert(dbContext, paramPerson);
            Assert.True(result.Id > 0);
            ContactPerson? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.True(result.Equals(readerResult));
            readerResult.FullName = "Cupen";
            await repo.DeleteById(dbContext, readerResult.Id);
            
            readerResult = await repo.FindById(dbContext, result.Id);
            Assert.Null(readerResult);

            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void UpdateTest()
        {
            DateTime now = DateTime.Now;
            var paramPerson = new ContactPerson() {
                FullName = "Nael",
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            ContactPersonRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(System.Data.ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            ContactPerson result = await repo.Insert(dbContext, paramPerson);
            Assert.True(result.Id > 0);
            ContactPerson? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.True(result.Equals(readerResult));
            readerResult.FullName = "Cupen";
            await repo.Update(dbContext, readerResult);
            
            readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.Equal("Cupen", readerResult.FullName);
            

            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }
    }
}