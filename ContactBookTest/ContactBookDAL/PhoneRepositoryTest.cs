using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.Entities;
using ContactBookDAL.MSQL;
using Microsoft.Data.SqlClient;
using Xunit.Abstractions;

namespace ContactBookTest.ContactBookDAL
{
    public class PhoneRepositoryTest
    {
        
        private DatabaseConfig _dbConfig;

        private readonly ITestOutputHelper _output;


        public PhoneRepositoryTest(ITestOutputHelper output)
        {
            _dbConfig = new DatabaseConfig("server=NDS-LPT-0462\\SQL2019;Database=ContactBookDB;User Id=nael_projects_admin;Password=Nael2024;Integrated Security=True;MultipleActiveResultSets=True;TrustServerCertificate=True;");
            _output = output;
        }

        [Fact]
        public async void TestInsert()
        {
            DateTime now = DateTime.Now;
            var paramPhone = new PhoneContact{
                PhoneNumber = "123456789",
                Description = "Test Phone Contact",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            PhoneRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            PhoneContact result = await repo.Insert(dbContext, paramPhone);
            Assert.True(result.Id > 0);

            PhoneContact? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.Equal(readerResult.Id, paramPhone.Id);
            Assert.Equal(readerResult.PhoneNumber, paramPhone.PhoneNumber);
            Assert.Equal(readerResult.Description, paramPhone.Description);
            Assert.Equal(readerResult.PersonId, paramPhone.PersonId);
            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }


        [Fact]
        public async void TestInsertBatch()
        {
            DateTime now = DateTime.Now;
            var paramPhone = new PhoneContact{
                PhoneNumber = "123456789",
                Description = "Test Phone Contact",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            var paramPhone2 = new PhoneContact{
                PhoneNumber = "0987413234",
                Description = "Test Phone Contact",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            List<PhoneContact> phoneContacts = [paramPhone, paramPhone2];

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            PhoneRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);
            dbContext.BeginTransactionAsync();
            await repo.InsertBatch(dbContext, phoneContacts);

            List<PhoneContact> result = await repo.FindByPersonId(dbContext, 2);
            Assert.Equal(2, result.Count);
            
            PhoneContact? subResult1 = result.FirstOrDefault(p => p.PhoneNumber.Equals(paramPhone.PhoneNumber));
            Assert.NotNull(subResult1);
            Assert.Equal(paramPhone.PhoneNumber, subResult1.PhoneNumber);
            Assert.Equal(paramPhone.PersonId, subResult1.PersonId);
            Assert.Equal(paramPhone.Description, subResult1.Description);

            PhoneContact? subResult2 = result.FirstOrDefault(p => p.PhoneNumber.Equals(paramPhone2.PhoneNumber));
            Assert.NotNull(subResult2);
            Assert.Equal(paramPhone2.PhoneNumber, subResult2.PhoneNumber);
            Assert.Equal(paramPhone2.PersonId, subResult2.PersonId);
            Assert.Equal(paramPhone2.Description, subResult2.Description);

            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void DeleteTest()
        {
            DateTime now = DateTime.Now;
            var paramPhone = new PhoneContact{
                PhoneNumber = "123456789",
                Description = "Test Phone Contact",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            PhoneRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            PhoneContact result = await repo.Insert(dbContext, paramPhone);
            Assert.True(result.Id > 0);

            PhoneContact? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.True(result.Equals(readerResult));

            await repo.DeleteById(dbContext, result.Id);

            readerResult = await repo.FindById(dbContext, result.Id);
            Assert.Null(readerResult);

            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }

        [Fact]
        public async void UpdateTest()
        {
            DateTime now = DateTime.Now;
            var paramPhone = new PhoneContact{
                PhoneNumber = "123456789",
                Description = "Test Phone Contact",
                PersonId = 2,
                CreatedAt = now,
                UpdatedAt = now
            };

            SqlConnection connection = _dbConfig.CreateSqlConnection();
            DbContext dbContext = new(connection);
            PhoneRepository repo = new();
            await dbContext.OpenConnectionAsync();
            Assert.Equal(ConnectionState.Open, dbContext.Connection.State);

            dbContext.BeginTransactionAsync();

            PhoneContact result = await repo.Insert(dbContext, paramPhone);
            Assert.True(result.Id > 0);

            PhoneContact? readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);
            Assert.True(result.Equals(readerResult));
            readerResult.PhoneNumber = "999999999999";
            
            await repo.Update(dbContext, readerResult);

            readerResult = await repo.FindById(dbContext, result.Id);
            Assert.NotNull(readerResult);

            Assert.Equal("999999999999", readerResult.PhoneNumber);

            await dbContext.RollbackAsync();
            dbContext.DisposeAsync();
        }
    }
}