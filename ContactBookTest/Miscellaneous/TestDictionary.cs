using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactBookDAL.Contracts.DTOs.Pagination.Enums;
using ContactBookDAL.Contracts.DTOs.PhoneContact;

namespace ContactBookTest.Miscellaneous
{
    public class TestDictionary
    {
        [Fact]
        public void KeyNotFound() 
        {
            var dict1 = new Dictionary<string, string>();

            dict1.Add("satu", "Apel");
            dict1.Add("dua", "Semangka");
            dict1.Add("tiga", "Mangga");

            Assert.Throws<KeyNotFoundException>(() => {
                _ = dict1["Test"];
            });
        }

        [Fact]
        public void TestEnumInterpolation()
        {
            string test = @$"{AppSortOrder.ASC}";
            Assert.Equal("ASC", test);
        }
    }
}