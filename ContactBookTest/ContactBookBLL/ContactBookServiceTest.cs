using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ContactBookBLL;
using ContactBookDAL.Contracts;
using ContactBookDAL.Contracts.Configs;
using ContactBookDAL.Contracts.DTOs.Address;
using ContactBookDAL.Contracts.DTOs.Pagination;
using ContactBookDAL.Contracts.DTOs.Pagination.Enums;
using ContactBookDAL.Contracts.DTOs.Person;
using ContactBookDAL.Contracts.DTOs.PhoneContact;
using ContactBookDAL.Contracts.Entities;
using ContactBookDAL.MSQL;
using Microsoft.Data.SqlClient;
using Moq;

namespace ContactBookTest.ContactBookBLL
{
    public class ContactBookServiceTest
    {
        private readonly string _connectionString;

        private readonly Mock<IDatabaseConfig> _mockDbConfig;

        private readonly Mock<IContactPersonRepository> _mockContactRepo;

        private readonly Mock<IAddressRepository> _mockAddressRepo;

        private readonly Mock<IPhoneRepository> _mockPhoneRepo;

        private readonly ContactService _contactService;

        public ContactBookServiceTest()
        {
            _connectionString = "server=NDS-LPT-0462\\SQL2019;Database=ContactBookDB;User Id=nael_projects_admin;Password=Nael2024;Integrated Security=True;MultipleActiveResultSets=True;TrustServerCertificate=True;";
            _mockDbConfig = new Mock<IDatabaseConfig>();
            _mockContactRepo = new Mock<IContactPersonRepository>();
            _mockAddressRepo = new Mock<IAddressRepository>();
            _mockPhoneRepo = new Mock<IPhoneRepository>();

            _contactService = new ContactService(
                _mockDbConfig.Object,
                _mockAddressRepo.Object,
                _mockPhoneRepo.Object,
                _mockContactRepo.Object
            );
        }

        // public async void 

        [Fact]
        public async void TestPagination()
        {
            var now = DateTime.Now;
            var totalRowScheme = 18;
            var requestPagination = new PersonPaginationQuery
            {
                Limit = 3,
                Page = 2,
                Keyword= "Test",
                Order = AppSortOrder.ASC,
                SortBy = "ID"
            };
            var totalPage = (long) Math.Ceiling((decimal) totalRowScheme / requestPagination.Limit);

            List<ContactPerson> responsePersonContacts = [];
            for(int i = 0 ; i < requestPagination.Limit ; i++) {
                responsePersonContacts.Add(new ContactPerson
                {
                    Id = i + 1
                    ,FullName = "Test " + i
                    ,CreatedAt = now
                    ,UpdatedAt = now
                });
            }

            var expectedResult = new ResponsePagination<ResponsePersonContact>()
                {
                    Page = 2
                    ,TotalPage = totalPage
                    ,TotalItem = totalRowScheme
                    ,Items = responsePersonContacts.Select(m => new ResponsePersonContact
                        {
                            Id = m.Id
                            ,FullName = m.FullName
                        }).ToList()
                };

            _mockDbConfig.Setup(m => m.CreateSqlConnection())
                .Returns(new SqlConnection(_connectionString));

            _mockContactRepo.Setup(m => m.CountTotalRows(It.IsAny<DbContext>()))
                .ReturnsAsync(totalRowScheme);

            _mockContactRepo.Setup(m => m.FindAll(It.IsAny<DbContext>(), It.IsAny<PersonPaginationQuery>()))
                .ReturnsAsync(responsePersonContacts);
            
            var result = await _contactService.GetContactPaginated(requestPagination);

            Assert.NotNull(result);

            Assert.Equal(expectedResult.TotalPage, result.TotalPage);
            Assert.Equal(expectedResult.Page, result.Page);
            Assert.Equal(expectedResult.Items, result.Items);

            _mockContactRepo.Verify(m => m.CountTotalRows(It.IsAny<DbContext>()), Times.Once);
            _mockContactRepo.Verify(m => m.FindAll(It.IsAny<DbContext>(), It.IsAny<PersonPaginationQuery>()), Times.Once);
        }

        [Fact]
        public async void TestInsert()
        {
            var requestPersonContact = new RequestPersonContact
            {
                FullName = "John Doe"
                ,Phones = new List<RequestPhone>
                {
                    new RequestPhone { PhoneNumber = "12345", Description = "Mobile" }
                }
                ,Addresses = new List<RequestAddress>
                {
                    new RequestAddress { PostalCode = "12345", Addresses = "123 Main St", City = "Bandung" }
                }
            };

            var now = DateTime.Now;

            var contactPerson = new ContactPerson
            {
                Id = 1
                ,FullName = requestPersonContact.FullName
                ,CreatedAt = now
                ,UpdatedAt = now
            };

            _mockDbConfig.Setup(m => m.CreateSqlConnection())
                .Returns(new SqlConnection(_connectionString));

            _mockContactRepo.Setup(m => m.Insert(It.IsAny<DbContext>(), It.IsAny<ContactPerson>()))
                .ReturnsAsync(contactPerson);

            _mockPhoneRepo.Setup(m => m.InsertBatch(It.IsAny<DbContext>(), It.IsAny<List<PhoneContact>>()))
                .Returns(Task.CompletedTask);

            _mockAddressRepo.Setup(m => m.InsertBatch(It.IsAny<DbContext>(), It.IsAny<List<Address>>()))
                .Returns(Task.CompletedTask);

            var result = await _contactService.InsertContact(requestPersonContact);

            Assert.NotNull(result);

            Assert.Equal(contactPerson.Id, result.Id);
            Assert.Equal(contactPerson.FullName, result.FullName);
            Assert.Equal(contactPerson.CreatedAt, result.CreatedAt);

            // method Verify digunakan untuk mengecek berapa kali method "Insert/Update" milik repo dipanggil di dalam service yang ditest
            _mockContactRepo.Verify(m => m.Insert(It.IsAny<DbContext>(), It.IsAny<ContactPerson>()), Times.Once);
            _mockPhoneRepo.Verify(m => m.InsertBatch(It.IsAny<DbContext>(), It.IsAny<List<PhoneContact>>()), Times.Once);
             _mockAddressRepo.Verify(m => m.InsertBatch(It.IsAny<DbContext>(), It.IsAny<List<Address>>()), Times.Once);
        }

    }
}